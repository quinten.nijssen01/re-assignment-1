# Requirements Engineering Assignment 1

This assignment is made in several separate files. 
The base files are: 

* model-1-classifier
* model-2-classifier
* model-3-classifier

Those files pre-processes the data, builds a model, fits the model and it writes the result with the unlabled dataset and the model in the file to the locationo `data/output` with a name `model-x-results`. 

Finally those results will be combined and reviewed. This is all done in the following file: 

* combine-results

This file combines the results makes visualizations etc. Then it saves the final result to the `data/output/models-results.csv` file. 

The `data` directory is seperated into two sub directories, `input` and `output` this is done to make it more clear. All the data that we produce are saved in the `output` folder. 

Within the `report` folder you can see our .pdf report.

## How to use the C++ program
The scripts are the main program and do all the necessary stuff for the assignment. In addition we've made a C++ program in order to preprocess some data for the model 2 classifier. It's not manditiory to set this up because the results are exported to the file `data/input/preprocessedReviews.csv`. This file will be loaded within the second model / classifer. 

Everything regarding to the c++ program is within the directory `c++`. 

The C++ program is called `verwerkcommentaar.cpp`. This file depends on the file `stringoperaties.h`, so make sure this header file is in the same folder as `verwerkcommentaar.cpp`.
`verwerkcommentaar.cpp` should be compiled with Clang, G++ or similar compiler.

* To compile it with Clang, enter: `clang++ -std=c++14 -Wall -o verwerkcommentaar.exe verwerkcommentaar.cpp`
* To compile it with G++, enter: `g++ -std=c++14 -Wall -o verwerkcommentaar.exe verwerkcommentaar.cpp`

When the program has been compiled, you can run it with:
* Linux: `./verwerkcommentaar.exe [argument]`
* Windows: `verwerkcommentaar.exe [argument]`

The C++ program accepts one argument. That argument is the CSV-file that the program takes as input. If no arguments are given, the program will automatically search for a file named `lijst.txt` and use that file as input file instead. If `lijst.txt` does not exist while no argument is given, the program terminates with an error message.
The program reads all information from the input file and writes the intermediate results to a file named `geformatteerde_lijst.txt`. Then the program pauses for one second and uses the file `geformatteerde_lijst.txt` as input file while producing the final output file: `verwerkte_lijst.txt`

`geformatteerde_lijst.txt` is a file in which all intermediate endline characters have been removed, which means that one single row in the Excell file will be presented by exactly one line in `geformatteerde_lijst.txt`.
`verwerkte_lijst.txt` is a file in which:
  *  All non-requirements rows have been removed.
  *  All information from the row with the exception of the comment section and whether it is functional or non-functional has been removed.
  *  All redundant spacing has been removed (spaces before a non-space character of a line starts and multiple spaces between two words)
  *  All punctuation (capital letters, full stops, commas, colons, semicolons etcetera) and non-ASCII characters have been removed.

Since only `verwerkte_lijst.txt` is interesting, `geformatteerde_lijst.txt` can be ignored.