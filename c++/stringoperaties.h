/*
 * bool 	ZitHetErin		(string lijn, string argument)
 * string 	Omdraaien		(string lijn)
 * string 	VerwijderEnter	(string lijn)
 * kint 	VindBeginpunt	(string lijn, string argument)
 * kint 	VindEindpunt	(string lijn, string argument)
 * string 	GeefDeelString	(string lijn, kint beginpunt, kint eindpunt)
 * string 	VindEnVervang	(string lijn, string trefwoord, string vervang_door) 
 * bool 	LegeLijn		(string lijn)
*/

bool ZitHetErin(std::string lijn, std::string argument)
{
	for(unsigned short int i = 0, j = 0; i < lijn.size(); i++)
	{
		if(lijn[i] == argument[j]) { j++; if(j == argument.size()){ return true; } }
		else { j = 0; }
	}
	return false;
}

std::string Omdraaien(std::string lijn)
{
	std::string nw = "";
	for(short int i = lijn.size() - 1; i >= 0; i--) { nw += lijn[i]; }
	return nw;
}

std::string VerwijderEnter(std::string lijn)
{
	std::string nw = "";
	for(unsigned short int i = 0; i < lijn.size(); i++)
	{
		if(lijn[i] != '\n') { nw += lijn[i]; }
	}
	return nw;
}

unsigned short int VindBeginpunt(std::string lijn, std::string argument)
{
	for(unsigned short int i = 0, j = 0; i < lijn.size(); i++)
	{
		if(lijn[i] == argument[j]) { j++; if(j == argument.size()){ return i+1 - argument.size(); } }
		else { j = 0; }
	}
	return lijn.size();
}

unsigned short int VindEindpunt(std::string lijn, std::string argument)
{
	for(unsigned short int i = 0, j = 0; i < lijn.size(); i++)
	{
		if(lijn[i] == argument[j]) { j++; if(j == argument.size()){ return i; } }
		else { j = 0; }
	}
	return lijn.size();
}

std::string GeefDeelString(std::string lijn, unsigned short int beginpunt, unsigned short int eindpunt)
{
	if(beginpunt > eindpunt) { unsigned short int x = beginpunt; beginpunt = eindpunt; eindpunt = x; }
	std::string nieuwe_lijn = "";
	for(unsigned short int i = beginpunt; i <= eindpunt; i++)
	{
		nieuwe_lijn += lijn[i];
	}
	return nieuwe_lijn;
}

std::string VindEnVervang(std::string lijn, std::string trefwoord, std::string vervang_door)
{
	if(lijn.size() < trefwoord.size()) { return lijn; }
	
	std::string nieuwe_lijn = "";
	for(unsigned short int i = 0; i < lijn.size(); i++)
	{
		if(lijn[i] == trefwoord[0] && lijn[i+trefwoord.size()-1] == trefwoord[trefwoord.size()-1])
		{
			std::string s = GeefDeelString(lijn, i, i+trefwoord.size()-1);
			if(s == trefwoord) { nieuwe_lijn += vervang_door; i += trefwoord.size()-1; }
			else { nieuwe_lijn += lijn[i]; }
		}
		else { nieuwe_lijn += lijn[i]; }
	}
	
	return nieuwe_lijn;
}

bool LegeLijn(std::string lijn)
{
	for(unsigned short int i = 0; i < lijn.size(); i++)
	{
		if(lijn[i] != ' ' && lijn[i] != '\t') { return false; }
	}
	return true;
}
