#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string>
#include "stringoperaties.h"
//#include "vector.h"
//#include "splitter.h"
using namespace std;

typedef unsigned short int kint; bool ontbeest = true;
#define log(x) if(ontbeest) { cout << x << endl; }

const long int d = 1000000; // miljoen
bool commentaar = false;
bool inVerwerkingslijn = false;

bool Puntkommas(string code, kint limiet)
{
	kint teller = 0;
	for(kint i = 0; i < code.size(); i++)
	{
		if(code[i] == '"') { commentaar = !commentaar; continue; }
		if(code[i] == ';' && !commentaar) { teller++; }
	}
	
	return (teller >= limiet);
}

string VerwijderOverbodigeSpaties(string code)
{
	string ns = "";
	bool teken_gezien = false;
	
	for(kint i = 0; i < code.size(); i++)
	{
		if(code[i] != ' ') { teken_gezien = true; ns += code[i]; continue; }
		if(teken_gezien && code[i] == ' ' && code[i-1] != ' ') { ns += code[i]; }
	}
	
	return ns;
}

string VerkrijgCommentaar(string code)
{
	string ns = "";
	kint teller = 0; 
		
	for(kint i = 0; i < code.size(); i++)
	{
		if(code[i] == '"') { commentaar = !commentaar; continue; }
		else if(code[i] == ';') { teller++; continue; }
		
		if(commentaar || teller == 9) { ns += code[i]; }
	}
	
	return ns;
}

string VerwijderInterpunctie(string code)
{
	string ns = "";
	
	for(kint i = 0; i < code.size(); i++)
	{
		if(code[i] >= 'A' && code[i] <= 'Z') { ns += char(code[i] + 32); }
		else if((code[i] >= 'a' && code[i] <= 'z') || code[i] == ' ') { ns += code[i]; }
	}
	
	return ns;
}

void VerwerkAlleGegevens()
{
	string lijn = "";
	string invoerbestand = "geformatteerde_lijst.txt";
	string uitvoerbestand = "verwerkte_lijst.txt";
	
	cout << "Uitvoerbestand: " << uitvoerbestand << endl;
	
	ifstream bestand(invoerbestand);
	ofstream uitvoer(uitvoerbestand);
	
	if(bestand.is_open())
	{
		while(getline(bestand, lijn))
		{
			string commentaar = VerkrijgCommentaar(lijn);
			commentaar = VerwijderInterpunctie(commentaar);
			commentaar = VerwijderOverbodigeSpaties(commentaar);
			string functional = "";
			
			if(ZitHetErin(lijn, "requirement"))
			{
				if(ZitHetErin(lijn, "non-functional")) { functional = "non-functional"; }
				else if(ZitHetErin(lijn, "functional")) { functional = "functional"; }
			}
			
			if(functional != "") { uitvoer << commentaar << ";" << functional << endl; }
		}
		bestand.close();
		uitvoer.close();
	}
	else
	{
		cout << "Kon " << invoerbestand << " niet vinden!" << endl;
	}
}

int main(int argc, char* argv[])
{
	string lijn = "";
	string invoerbestand = "lijst.txt";
	string uitvoerbestand = "geformatteerde_lijst.txt";
	
	if(argc >= 2) { invoerbestand = argv[1]; }
	cout << "Invoerbestand: " << invoerbestand << endl;
	cout << "Tijdelijk uitvoerbestand: " << uitvoerbestand << endl;
	
	ifstream bestand(invoerbestand);
	ofstream uitvoer(uitvoerbestand);
	if(bestand.is_open())
	{
		while(getline(bestand, lijn))
		{
			string nw = "";
			nw = VerwijderEnter(lijn);
			
			if(!inVerwerkingslijn)
			{
				if(Puntkommas(lijn, 10)) { uitvoer << nw << endl; }
				else { uitvoer << nw << " "; inVerwerkingslijn = true; }
			}
			else
			{
				if(Puntkommas(lijn, 4))
				{ uitvoer << nw << endl; inVerwerkingslijn = false; }
				else { uitvoer << nw << " "; }
			}
		}
		bestand.close();
		uitvoer.close();
	}
	else
	{
		cout << "Kon " << invoerbestand << " niet vinden!" << endl;
	}
	
	usleep(d);
	VerwerkAlleGegevens();
	
	return 0;
}
